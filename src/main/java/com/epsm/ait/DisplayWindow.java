package com.epsm.ait;

import java.awt.*;
import java.util.ArrayList;

public class DisplayWindow extends Canvas {
    @Override
    public void paint(Graphics g) {
        //draw the init point
        double xWInit = 500 * Main.xInit.X / Main.XLIMIT;
        double yWInit = 500 * Main.xInit.Y / Main.YLIMIT;
        xWInit += 500;
        yWInit += 500;
        yWInit = 1000 - yWInit;
        g.setColor(Color.green);
        g.fillOval((int) xWInit, (int) yWInit, 5, 5);
        //-------------------

        //draw the goals
        for (Vertex i : Main.xGoal) {
            double xW = 500 * i.X / Main.XLIMIT;
            double yW = 500 * i.Y / Main.YLIMIT;
            xW += 500;
            yW += 500;
            yW = 1000 - yW;
            g.setColor(Color.red);
            g.fillOval((int) xW, (int) yW, 5, 5);
        }
        //--------------

        //draw the samples
        for (Vertex i : Main.xSampled) {
            if (!i.equals(Main.xInit) && !Main.xGoal.contains(i)) {
                double xW = 500 * i.X / Main.XLIMIT;
                double yW = 500 * i.Y / Main.YLIMIT;
                xW += 500;
                yW += 500;
                yW = 1000 - yW;
                g.setColor(Color.blue);
                g.fillOval((int) xW, (int) yW, 5, 5);
            }

        }
        //----------------

        //draw the forward tree
        for (Edge e : Main.f.E) {
            double xWp = 500 * e.parent.X / Main.XLIMIT;
            double yWp = 500 * e.parent.Y / Main.YLIMIT;
            xWp += 500;
            yWp += 500;
            yWp = 1000 - yWp;
            double xWc = 500 * e.child.X / Main.XLIMIT;
            double yWc = 500 * e.child.Y / Main.YLIMIT;
            xWc += 500;
            yWc += 500;
            yWc = 1000 - yWc;
            g.setColor(Color.darkGray);
            g.drawLine((int) xWp, (int) yWp, (int) xWc, (int) yWc);
        }
        //---------------------
        //draw the reverse tree
//        for (Edge e : Main.r.E) {
//            double xWp = 500 * e.parent.X / Main.XLIMIT;
//            double yWp = 500 * e.parent.Y /Main.YLIMIT;
//            xWp += 500;
//            yWp += 500;
//            yWp = 1000 - yWp;
//            double xWc = 500 * e.child.X / Main.XLIMIT;
//            double yWc = 500 * e.child.Y /Main.YLIMIT;
//            xWc += 500;
//            yWc += 500;
//            yWc = 1000 - yWc;
//            g.setColor(Color.darkGray);
//            g.drawLine((int)xWp, (int)yWp, (int)xWc, (int)yWc);
//        }
//        //---------------------
        //draw the obstacles
        for (Rectangle r : Main.obstacles) {
            double xW = 500 * r.x / Main.XLIMIT;
            double yW = 500 * r.y / Main.YLIMIT;
            xW += 500;
            yW += 500;
            yW = 1000 - yW;
            double w = 1000 * r.w / (2 * Main.YLIMIT);
            double h = 1000 * r.h / (2 * Main.XLIMIT);
            g.setColor(Color.BLACK);
            g.fillRect((int) xW, (int) yW, (int) w, (int) h);
        }
        //------------------
        //draw all the optimal routes
        for (ArrayList<Edge> al : Main.trails) {
            for (Edge e : al) {
                double xWp = 500 * e.parent.X / Main.XLIMIT;
                double yWp = 500 * e.parent.Y / Main.YLIMIT;
                xWp += 500;
                yWp += 500;
                yWp = 1000 - yWp;
                double xWc = 500 * e.child.X / Main.XLIMIT;
                double yWc = 500 * e.child.Y / Main.YLIMIT;
                xWc += 500;
                yWc += 500;
                yWc = 1000 - yWc;
                g.setColor(Color.magenta);
                g.drawLine((int) xWp, (int) yWp, (int) xWc, (int) yWc);
            }
        }
        //-------------------------
        //display current solution
        g.setColor(Color.BLACK);
        g.drawString("Best solutions so far:", 1020, 20);
        int scoreLocation = 50;
        for (ArrayList<Edge> al : Main.trails) {
            double score = 0;
            for (Edge e : al) {
                score += Main.c(e.parent, e.child);
            }
            g.drawString(Main.xInit + " -> " + al.get(0).child.toString() + " : " + score, 1020, scoreLocation);
            scoreLocation += 30;
        }
        //------------------------
    }
}
