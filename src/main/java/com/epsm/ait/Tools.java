package com.epsm.ait;

import java.util.ArrayList;
import java.util.HashSet;

public class Tools {
    public static ArrayList<Vertex> eliminateDuplicateVertices(ArrayList<Vertex> list) {
        return new ArrayList<>(new HashSet<>(list));
    }

    public static boolean doEdgesWithUnprocessedVerticesExistInQR() {
        return Main.qF.parallelStream()
                .anyMatch(e -> !Vertex.processingStatus.get(e.parent) || !Vertex.processingStatus.get(e.child));
    }

    public static Vertex popBestQR() {
        Vertex x = KeyR.minimumVertexByKeyInQR();
        Main.qR.remove(x);
        return x;
    }

    public static Edge popBestQF() {
        Edge e = KeyF.minimumVertexByKeyInQF();
        Main.qF.remove(e);
        return e;
    }
}
