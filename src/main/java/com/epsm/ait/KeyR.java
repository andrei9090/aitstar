package com.epsm.ait;

import java.util.Comparator;
import java.util.Optional;

public class KeyR implements Comparable<KeyR> {
    public double criteria1;
    public double criteria2;

    public KeyR(double criteria1, double criteria2) {
        this.criteria1 = criteria1;
        this.criteria2 = criteria2;
    }

    public static KeyR computeKeyR(Vertex x) {
        if (x == null)
            return new KeyR(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        return new KeyR(Math.min(Main.hCon.getOrDefault(x, Double.POSITIVE_INFINITY),
                Main.hExp.getOrDefault(x, Double.POSITIVE_INFINITY)) + Main.g(x),
                Math.min(Main.hCon.getOrDefault(x, Double.POSITIVE_INFINITY),
                        Main.hExp.getOrDefault(x, Double.POSITIVE_INFINITY)));
    }

    public static Vertex minimumVertexByKeyInQR() {
        Optional<Vertex> result = Main.qR.parallelStream().min(Comparator.comparing(KeyR::computeKeyR));
        return result.orElse(null);
    }

    @Override
    public int compareTo(KeyR keyR) {
        if (Double.compare(this.criteria1, keyR.criteria1) != 0) {
            return Double.compare(this.criteria1, keyR.criteria1);
        } else {
            return Double.compare(this.criteria2, keyR.criteria2);
        }
    }
}
