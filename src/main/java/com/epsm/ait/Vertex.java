package com.epsm.ait;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class Vertex {
    public static ConcurrentHashMap<Vertex, Boolean> processingStatus = new ConcurrentHashMap<>();
    public double X;
    public double Y;

    public Vertex(double X, double Y) {
        this.X = X;
        this.Y = Y;
        processingStatus.putIfAbsent(this, false);
    }

    public double euclideanDistance2D(Vertex v) {
        return Math.sqrt((v.X - this.X) * (v.X - this.X) +
                (v.Y - this.Y) * (v.Y - this.Y));
    }

    public ArrayList<Vertex> neighbors() {
        ArrayList<Vertex> vNeighbours =
                new ArrayList<>(Main.xSampled.parallelStream()
                        .filter(i -> i.euclideanDistance2D(this) <= Main.r()).toList());

        Vertex pF = Main.f.parent(this);
;
        if (pF != null) {
            vNeighbours.add(pF);
        }

        vNeighbours.addAll(Main.f.children(this));

        vNeighbours = Tools.eliminateDuplicateVertices(vNeighbours);

        vNeighbours.removeIf(vertex -> Edge.isEdgeInvalid(vertex, this) || Edge.isEdgeInvalid(this, vertex));
        return vNeighbours;
    }

    public ArrayList<Edge> expand() {
        ArrayList<Edge> eOut = new ArrayList<>();
        ArrayList<Vertex> nb = neighbors();
        for (Vertex x : nb) {
            eOut.add(new Edge(this, x));
        }
        processingStatus.put(this, true);
        return eOut;
    }

    @Override
    public boolean equals(Object v) {
        if (!(v instanceof Vertex)) {
            return false;
        } else {
            return ((Vertex) v).X == this.X && ((Vertex) v).Y == this.Y;
        }
    }

    @Override
    public String toString() {
        return "{" + X + "," + Y + "}";
    }
}
