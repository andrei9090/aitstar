package com.epsm.ait;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    final public static double XLIMIT = 6;
    final public static double YLIMIT = 6;
    final public static double NIU = 1.001;
    final public static int M = 100;
    public static SearchTree f = new SearchTree();
    public static int numOfSamples = 0;
    public static ArrayList<Vertex> xSampled = new ArrayList<>();
    public static ArrayList<Edge> qF = new ArrayList<>();
    public static ArrayList<Vertex> qR = new ArrayList<>();
    public static ConcurrentHashMap<Vertex, Double> hCon = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<Vertex, Double> hExp = new ConcurrentHashMap<>();
    public static ArrayList<Vertex> xGoal = new ArrayList<>();
    public static Vertex xInit;
    public static ArrayList<Edge> eInvalid = new ArrayList<>();
    public static Random rg = new Random();
    public static JFrame frame = new JFrame("Algorithm visualization");
    public static Canvas canvas = new DisplayWindow();
    public static ArrayList<Rectangle> obstacles = new ArrayList<>();
    public static ArrayList<ArrayList<Edge>> trails = new ArrayList<>();
    public static double cCurrent = Double.POSITIVE_INFINITY;

    public static double r() {
        double lambda =
                cCurrent == Double.POSITIVE_INFINITY ?
                        (4 * XLIMIT * YLIMIT) :
                        (Math.pow(cCurrent / 2, 2) * Math.PI);
        int q = xSampled.size();
        return Main.NIU * Math.pow(3 * (lambda / Math.PI) * (Math.log(q) / q), 0.5);
    }

    public static void update_heuristic(Edge e) {
        if (e == null) {
            for (Vertex x : Main.qR) {
                hCon.put(x, Double.POSITIVE_INFINITY);
                hExp.put(x, Double.POSITIVE_INFINITY);
            }
            for (Vertex x : Main.xGoal) {
                hCon.put(x, 0.0);
                qR.add(x);
            }
        } else {
            eInvalid.add(e);
            update_state(e.parent);
        }
        while (KeyR.computeKeyR(KeyR.minimumVertexByKeyInQR()).compareTo(KeyR.computeKeyR(xInit)) < 0 ||
                hExp.getOrDefault(xInit, Double.POSITIVE_INFINITY) < hCon.getOrDefault(xInit, Double.POSITIVE_INFINITY)
                || Tools.doEdgesWithUnprocessedVerticesExistInQR()) {
            Vertex x = Tools.popBestQR();
            if (x == null)
                break;
            if (hCon.getOrDefault(x, Double.POSITIVE_INFINITY) < hExp.getOrDefault(x, Double.POSITIVE_INFINITY)) {
                hExp.put(x, hCon.getOrDefault(x, Double.POSITIVE_INFINITY));
            } else {
                hExp.put(x, Double.POSITIVE_INFINITY);
                update_state(x);
            }
            ArrayList<Vertex> nb = x.neighbors();
            for (Vertex i : nb) {
                update_state(i);
            }
        }
    }

    public static void update_state(Vertex x) {
        if (!x.equals(xInit)) {
            ArrayList<Vertex> nb = x.neighbors();
            Vertex xP = null;
            Optional<Vertex> result = nb.parallelStream()
                    .min(Comparator.comparingDouble(xi -> hExp.getOrDefault(xi, Double.POSITIVE_INFINITY) + c(xi, x)));
            if (result.isPresent()) {
                xP = result.get();
            }
            hCon.put(x, hExp.getOrDefault(xP, Double.POSITIVE_INFINITY) + c(xP, x));
            if (!hCon.get(x).equals(hExp.get(x))) {
                if (!qR.contains(x)) {
                    qR.add(x);
                }
            } else {
                qR.remove(x);
            }
        }
    }

    public static double c(Vertex x1, Vertex x2) {
        return x1.euclideanDistance2D(x2);
    }

    public static double g(Vertex x) {
        return xInit.euclideanDistance2D(x);
    }

    public static double h(Vertex x) {
        //return Main.xGoal.parallelStream().map(x::euclideanDistance2D).min(Double::compare).get();
        return Math.min(hExp.getOrDefault(x, Double.POSITIVE_INFINITY), hCon.getOrDefault(x, Double.POSITIVE_INFINITY));
    }

    public static void main(String[] args) {
        String s = (String) JOptionPane.showInputDialog(
                frame,
                "Choose one of the examples for the algorithm to use",
                "Example choice",
                JOptionPane.PLAIN_MESSAGE,
                null,
                ChoiceDialog.options,
                "Example 1");
        if (s == null) {
            System.exit(0);
        }
        switch (s) {
            case "Example 1" -> Examples.loadExample1();
            case "Example 2" -> Examples.loadExample2();
            case "Example 3" -> Examples.loadExample3();
            case "Example 4" -> Examples.loadExample4();
            case "Example 5" -> Examples.loadExample5();
        }
        //start initializare structuri
        f.V.add(xInit);

        xSampled.addAll(xGoal);
        numOfSamples += xGoal.size();
        if (!xSampled.contains(xInit)) {
            xSampled.add(xInit);
            numOfSamples++;
        }

        qF.addAll(xInit.expand());
        //end initializare structuri

        //start window init
        canvas.setSize(1600, 1000);
        frame.setSize(1600, 1000);
        JButton restart = new JButton();
        restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                StringBuilder cmd = new StringBuilder();
                cmd.append(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java ");
                for (String jvmArg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
                    cmd.append(jvmArg + " ");
                }
                cmd.append("-cp ").append(ManagementFactory.getRuntimeMXBean().getClassPath()).append(" ");
                cmd.append(Main.class.getName()).append(" ");
                for (String arg : args) {
                    cmd.append(arg).append(" ");
                }
                try {
                    Runtime.getRuntime().exec(cmd.toString());
                } catch (Exception e) {

                } finally {
                    System.exit(0);
                }
            }
        });
        restart.setBounds(1020, 950, 100, 50);
        restart.setText("Restart");
        frame.add(restart);
        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //end window init

        update_heuristic(null);
        while (true) {
            Edge e = Tools.popBestQF();
            if (e == null) {
                ArrayList<Vertex> newSamples = sample(M, cCurrent);
                numOfSamples += newSamples.size();
                xSampled.addAll(newSamples);
                xSampled = new ArrayList<>(xSampled.parallelStream()
                        .filter(p -> {
                            double minp = Double.POSITIVE_INFINITY;
                            for (Vertex xg : xGoal) {
                                if (g(p) + p.euclideanDistance2D(xg) < minp) {
                                    minp = g(p) + p.euclideanDistance2D(xg);
                                }
                            }
                            return minp <= cCurrent;
                        }).toList());
                canvas.repaint();
                qF.addAll(xInit.expand());
                update_heuristic(null);
                continue;
            }
            if (SearchTree.gF(e.parent) + c(e.parent, e.child) + h(e.child) < cCurrent) {
                if (f.E.contains(e)) {
                    qF.addAll(e.child.expand());
                } else if (SearchTree.gF(e.parent) + c(e.parent, e.child) < SearchTree.gF(e.child)) {
                    if (!eInvalid.contains(e) && !Rectangle.isEdgeCrossingObstacles(e)) { //asta banuiesc ca inseamna is_valid
                        if (!f.V.contains(e.child)) {
                            f.V.add(e.child);
                        } else {
                            f.E.remove(new Edge(f.parent(e.child), e.child));
                        }
                        f.E.add(e);
                        //frame.repaint();
                        qF.addAll(e.child.expand());
                        cCurrent = xGoal.parallelStream().map(SearchTree::gF).min(Double::compare).get();
                        System.out.println("[" + LocalDateTime.now() + "]" + cCurrent);
                        trails.clear();
                        for (Vertex xg : xGoal) {
                            if (f.V.contains(xg)) {
                                ArrayList<Edge> trail = SearchTree.getTrail(xg);
                                trails.add(trail);
                                System.out.println(trail);
                            }
                        }
                        canvas.repaint();
                    } else {
                        update_heuristic(e);
                    }
                }
            } else {
                ArrayList<Vertex> newSamples = sample(M, cCurrent);
                numOfSamples += newSamples.size();
                xSampled.addAll(newSamples);
                xSampled = new ArrayList<>(xSampled.parallelStream()
                        .filter(p -> {
                            double minp = Double.POSITIVE_INFINITY;
                            for (Vertex xg : xGoal) {
                                if (g(p) + p.euclideanDistance2D(xg) < minp) {
                                    minp = g(p) + p.euclideanDistance2D(xg);
                                }
                            }
                            return minp <= cCurrent;
                        }).toList());
                canvas.repaint();
                qF.addAll(xInit.expand());
                update_heuristic(null);
            }

        }
    }

    public static ArrayList<Vertex> sample(int m, double cCurrent) {
        ArrayList<Vertex> newSamples = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            while (true) {
                boolean signX;
                double numberX;
                signX = rg.nextBoolean();
                numberX = rg.nextDouble();
                numberX = numberX * XLIMIT * (signX ? -1 : 1);

                boolean signY;
                double numberY;
                signY = rg.nextBoolean();
                numberY = rg.nextDouble();
                numberY = numberY * YLIMIT * (signY ? -1 : 1);

                Vertex p = new Vertex(numberX, numberY);

                if (g(p) + Main.xGoal
                        .parallelStream()
                        .map(p::euclideanDistance2D)
                        .min(Double::compare)
                        .get() <= cCurrent && !Rectangle.isPointInObstacle(p)) {
                    newSamples.add(p);
                    break;
                }
            }

        }
        return newSamples;
    }
}
