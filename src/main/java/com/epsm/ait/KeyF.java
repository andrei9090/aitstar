package com.epsm.ait;

import java.util.Comparator;
import java.util.Optional;

public class KeyF implements Comparable<KeyF> {
    public double criteria1;
    public double criteria2;
    public double criteria3;

    public KeyF(double criteria1, double criteria2, double criteria3) {
        this.criteria1 = criteria1;
        this.criteria2 = criteria2;
        this.criteria3 = criteria3;
    }

    public static KeyF computeKeyF(Edge e) {
        Vertex xP = e.parent;
        Vertex xC = e.child;
        return new KeyF(SearchTree.gF(xP) + Main.c(xP, xC) + Main.h(xC),
                SearchTree.gF(xP) + Main.c(xP, xC), SearchTree.gF(xP));
    }

    public static Edge minimumVertexByKeyInQF() {
        Optional<Edge> result = Main.qF.parallelStream().min(Comparator.comparing(KeyF::computeKeyF));
        return result.orElse(null);
    }

    @Override
    public int compareTo(KeyF keyF) {
        if (Double.compare(this.criteria1, keyF.criteria1) != 0) {
            return Double.compare(this.criteria1, keyF.criteria1);
        } else {
            if (Double.compare(this.criteria2, keyF.criteria2) != 0)
                return Double.compare(this.criteria2, keyF.criteria2);
            else
                return Double.compare(this.criteria3, keyF.criteria3);
        }
    }
}
