package com.epsm.ait;

import org.locationtech.jts.geom.*;

public class Rectangle {
    public double x;
    public double y;
    public double h;
    public double w;

    public Rectangle(double x, double y, double h, double w) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public static boolean isEdgeCrossingObstacles(Edge e) {
        boolean isCrossing = false;
        for (Rectangle r : Main.obstacles) {
            if (r.lineIntersectsRectangle(e)) {
                isCrossing = true;
                break;
            }
        }
        return isCrossing;
    }

    public static boolean isPointInObstacle(Vertex v) {
        boolean isIn = false;
        for (Rectangle r : Main.obstacles) {
            if (r.isVertexInRectangle(v)) {
                isIn = true;
                break;
            }
        }
        return isIn;
    }

    public boolean lineIntersectsRectangle(Edge e) {
        LineString segment = new GeometryFactory().createLineString(
                new Coordinate[]{
                        new Coordinate(e.parent.X, e.parent.Y),
                        new Coordinate(e.child.X, e.child.Y)
                });
        LineString latura1 = new GeometryFactory().createLineString(
                new Coordinate[]{
                        new Coordinate(x, y),
                        new Coordinate(x + w, y)
                });
        LineString latura2 = new GeometryFactory().createLineString(
                new Coordinate[]{
                        new Coordinate(x, y),
                        new Coordinate(x, y - h)
                });
        LineString latura3 = new GeometryFactory().createLineString(
                new Coordinate[]{
                        new Coordinate(x + w, y),
                        new Coordinate(x + w, y - h)
                });
        LineString latura4 = new GeometryFactory().createLineString(
                new Coordinate[]{
                        new Coordinate(x, y - h),
                        new Coordinate(x + w, y - h)
                });

        Point p1 = new GeometryFactory().createPoint(new Coordinate(e.parent.X, e.parent.Y));
        Point p2 = new GeometryFactory().createPoint(new Coordinate(e.child.X, e.child.Y));
        Polygon r = new GeometryFactory().createPolygon(new Coordinate[]{
                new Coordinate(x, y),
                new Coordinate(x + w, y),
                new Coordinate(x + w, y - h),
                new Coordinate(x, y - h),
                new Coordinate(x, y)
        });
        boolean intersectsRectangleLines = segment.intersects(latura1) ||
                segment.intersects(latura2) ||
                segment.intersects(latura3) ||
                segment.intersects(latura4);

        return intersectsRectangleLines || r.contains(p1) || r.contains(p2);
    }

    public boolean isVertexInRectangle(Vertex v) {
        Point p = new GeometryFactory().createPoint(new Coordinate(v.X, v.Y));
        Polygon r = new GeometryFactory().createPolygon(new Coordinate[]{
                new Coordinate(x, y),
                new Coordinate(x + w, y),
                new Coordinate(x + w, y - h),
                new Coordinate(x, y - h),
                new Coordinate(x, y)
        });
        return p.coveredBy(r);
    }
}
