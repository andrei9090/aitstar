package com.epsm.ait;

public class Examples {
    public static void loadExample1() {
        Main.xInit = new Vertex(0.2, 0.5);
        Main.xGoal.add(new Vertex(3, 5));
        Main.xGoal.add(new Vertex(3.9, 5));
        Main.xGoal.add(new Vertex(3.8, -4));
        Main.obstacles.add(new Rectangle(0.4, 6, 8, 0.3));
        Main.obstacles.add(new Rectangle(-6, 6, 12, 0.3));
        Main.obstacles.add(new Rectangle(-6, 6, 0.3, 12));
        Main.obstacles.add(new Rectangle(5.7, 6, 12, 0.4));
        Main.obstacles.add(new Rectangle(-6, -5.7, 0.4, 12));
        Main.obstacles.add(new Rectangle(2, 0, 8, 0.3));
        Main.obstacles.add(new Rectangle(2, 6, 4.5, 0.3));
        Main.obstacles.add(new Rectangle(2, 2.5, 0.3, 1.5));
        Main.obstacles.add(new Rectangle(-3, -1, 0.3, 3.7));
        Main.obstacles.add(new Rectangle(-3, 4.6, 8.7, 0.3));
        Main.obstacles.add(new Rectangle(-3, -1, 0.3, 3.7));
    }

    public static void loadExample2() {
        Main.xInit = new Vertex(-5, -5);
        Main.xGoal.add(new Vertex(3.9, 5));
        //borders
        Main.obstacles.add(new Rectangle(-6, -5.7, 0.4, 12));
        Main.obstacles.add(new Rectangle(-6, 6, 0.3, 12));
        Main.obstacles.add(new Rectangle(5.7, 6, 12, 0.4));
        Main.obstacles.add(new Rectangle(-6, 6, 12, 0.3));
        //--------
        Main.obstacles.add(new Rectangle(1.5, 1.5, 1, 1));
        Main.obstacles.add(new Rectangle(3, 1.5, 1, 1));
        Main.obstacles.add(new Rectangle(1, 3, 1, 2));
        Main.obstacles.add(new Rectangle(0, 4, 1, 1.5));
        Main.obstacles.add(new Rectangle(1.3, -2.6, 1, 1.5));
        Main.obstacles.add(new Rectangle(-6, 6, 12, 0.3));
        Main.obstacles.add(new Rectangle(3.4, 3.5, 1, 2));
        Main.obstacles.add(new Rectangle(-3, 4, 1, 2));
        Main.obstacles.add(new Rectangle(-2, 5.4, 1, 1.5));
        Main.obstacles.add(new Rectangle(2, 5.3, 1, 1.5));
        Main.obstacles.add(new Rectangle(-1, 1.5, 3, 1.5));
        Main.obstacles.add(new Rectangle(-3.2, 2, 2, 1.5));
        Main.obstacles.add(new Rectangle(-5, 4, 2, 1.5));
        Main.obstacles.add(new Rectangle(1.4, 0.1, 2, 1.5));
        Main.obstacles.add(new Rectangle(4, 0.1, 1, 2));
        Main.obstacles.add(new Rectangle(-4.5, -1.3, 1, 2));
        Main.obstacles.add(new Rectangle(-3, -3, 1, 1));
        Main.obstacles.add(new Rectangle(-1, -3, 1.5, 1.5));
        Main.obstacles.add(new Rectangle(3.6, -3, 1.5, 1.5));
    }

    public static void loadExample3() {
        Main.xInit = new Vertex(-1.5, 0);
        Main.xGoal.add(new Vertex(2, 0));
        //borders
        Main.obstacles.add(new Rectangle(-6, -5.7, 0.4, 12));
        Main.obstacles.add(new Rectangle(-6, 6, 0.3, 12));
        Main.obstacles.add(new Rectangle(5.7, 6, 12, 0.4));
        Main.obstacles.add(new Rectangle(-6, 6, 12, 0.3));
        //--------
        Main.obstacles.add(new Rectangle(0, 2, 4, 0.5));
        Main.obstacles.add(new Rectangle(-3, 2, 0.5, 3));
        Main.obstacles.add(new Rectangle(-3, -2, 0.5, 3.5));
    }

    public static void loadExample4() {
        Main.xInit = new Vertex(-1.5, 0);
        Main.xGoal.add(new Vertex(1, 0));
        //borders
        Main.obstacles.add(new Rectangle(-6, -5.7, 0.4, 12));
        Main.obstacles.add(new Rectangle(-6, 6, 0.3, 12));
        Main.obstacles.add(new Rectangle(5.7, 6, 12, 0.4));
        Main.obstacles.add(new Rectangle(-6, 6, 12, 0.3));
        //--------
        Main.obstacles.add(new Rectangle(0, 2, 4, 0.5));
        Main.obstacles.add(new Rectangle(0, 2, 0.5, 4));
        Main.obstacles.add(new Rectangle(0, -2, 0.5, 4));
    }

    public static void loadExample5() {
        Main.xInit = new Vertex(-4, 0);
        Main.xGoal.add(new Vertex(4, 0));
        //borders
        Main.obstacles.add(new Rectangle(-6, -5.7, 0.4, 12));
        Main.obstacles.add(new Rectangle(-6, 6, 0.3, 12));
        Main.obstacles.add(new Rectangle(5.7, 6, 12, 0.4));
        Main.obstacles.add(new Rectangle(-6, 6, 12, 0.3));
        //--------
        Main.obstacles.add(new Rectangle(-2, 2, 8, 4));
        Main.obstacles.add(new Rectangle(-2, 5.2, 2.6, 4));
    }
}
