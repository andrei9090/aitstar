package com.epsm.ait;

public class Edge {
    public Vertex parent;
    public Vertex child;

    public Edge(Vertex parent, Vertex child) {
        this.parent = parent;
        this.child = child;
    }

    static boolean isEdgeInvalid(Vertex v1, Vertex v2) {

        return Main.eInvalid.contains(new Edge(v1, v2)) || Rectangle.isEdgeCrossingObstacles(new Edge(v1, v2));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Edge)) {
            return false;
        } else {
            return ((Edge) o).child.equals(this.child) && ((Edge) o).parent.equals(this.parent);
        }
    }

    @Override
    public String toString() {
        return "{" + child + "," + parent + "}";
    }
}
