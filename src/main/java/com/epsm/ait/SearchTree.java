package com.epsm.ait;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epsm.ait.Main.f;

public class SearchTree {
    public ArrayList<Vertex> V;
    public ArrayList<Edge> E;

    public SearchTree() {
        V = new ArrayList<>();
        E = new ArrayList<>();
    }

    public static ArrayList<Edge> getTrail(Vertex x) {
        ArrayList<Edge> trail = new ArrayList<>();
        for (Edge e : f.E) {
            if (e.child.equals(x)) {
                trail.add(e);
                trail.addAll(getTrail(e.parent));
                break;
            }
        }
        return trail;
    }

    public static double gF(Vertex x) {
        if (!f.V.contains(x)) {
            return Double.POSITIVE_INFINITY;
        } else {
            List<Double> result = Main.f.E.stream().filter(e -> e.child.equals(x))
                    .map(e -> Main.c(e.parent, e.child) + gF(e.parent)).toList();
            if (result.size() == 0)
                return 0;
            else
                return result.get(0);
        }
    }

    public Vertex parent(Vertex x) {
        Optional<Edge> result = E.parallelStream().filter(i -> i.child.equals(x)).findFirst();
        return result.map(edge -> edge.parent).orElse(null);
    }

    public ArrayList<Vertex> children(Vertex x) {
        return new ArrayList<>(
                E.parallelStream().filter(i -> i.parent.equals(x)).map(i -> i.child).toList()
        );
    }
}
